from django.http import HttpResponse, JsonResponse
# Create your views here.
from django.shortcuts import render
from ticketing_app.models import TblTicket


def index(request):
    return render(request, 'home.html')


def showlayout(request):
    return render(request, 'applayout.html')


def home(request):
    return render(request, 'home.html')


def submit(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        body = request.POST.get('body')
        new_ticket = TblTicket(submitter=username, body=body)
        new_ticket.save()
        return HttpResponse("Successfully submitted ticket!")
    return render(request, 'submit.html')


def tickets_raw(request):
    all_tickets = list(TblTicket.objects.values())
    return JsonResponse(all_tickets, safe=False)


def tickets(request):
    all_tickets = TblTicket.objects.all()
    return render(request, 'tickets.html', {'tickets': all_tickets})


def admin(request):
    return render(request, 'admin.html')


def ticket(request, ticket_id):
    selected_ticket = TblTicket.objects.get(pk=ticket_id)
    return render(request, 'ticket.html', {'ticket': selected_ticket})
