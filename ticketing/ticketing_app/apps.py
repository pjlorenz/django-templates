from django.apps import AppConfig


class TicketingAppConfig(AppConfig):
    name = 'ticketing_app'
